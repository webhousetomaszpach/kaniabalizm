class Game {
  constructor() {

  }

  startGame() {
    kaniaMove();
  }
}

class GameSettings {
  constructor(diff, amount) {
    this.diff = diff;
    this.amount = amount;
  }

  difficultyLevel() {
      console.log(`Poziom gry ustawiony na: ${this.diff}.`);
      var $diff = document.getElementById('diff').getElementsByTagName('span')[0];

      $diff.innerText = this.diff;
  }

  amountValue() {
    console.log(`Ilosc ruchow ustawiona na: ${this.amount}.`);
  }
}

// Instance of that class (Game)
const gameSetting = new GameSettings(100, 5);

// Call game methods
gameSetting.difficultyLevel();
gameSetting.amountValue();

const game = new Game();

function kaniaMove() {
  var $game = document.getElementById('game'),
      $cube = document.getElementById('cube');

  for (var i = 1; i <= 2; i++) {
      (function (index) {
          setTimeout(function () {
              var x = Math.floor((Math.random() * 100) + 1),
                  y = Math.floor((Math.random() * 100) + 1),
                  r = Math.floor((Math.random() * 360) + 1);
              $cube.style.top = 'calc(' + y + 'vh - 10px)';
              $cube.style.left = 'calc(' + x + 'vw - 10px)';
              $cube.style.transform = 'rotate(' + r + 'deg)';

              var children = document.createElement('div');
              children.style.top = 'calc(' + y + 'vh - 10px)';
              children.style.left = 'calc(' + x + 'vw - 10px)';
              children.style.transform = 'rotate(' + r + 'deg)';
              children.classList.add('rotate');


              setTimeout(function () {
                  $game.appendChild(children);
              }, 450);

              if ($cube.style.top === 'calc(100vh - 10px)') {
                  $cube.style.top = '50vh';
              }
          }, i * 450);
      })(i);
  }
}

function playGame() {
  game.startGame();
}
